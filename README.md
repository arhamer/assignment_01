# README #

Contained in the repository is a MATLAB program that generates the ten anonymous functions:

Call price
Put price
Call delta
Put delta
Gamma
Vega
Call Theta
Put Theta
Call Rho
Put Rho

Input variables are as follows:
S = Underlying price per share in home currency
K = Strike price per share in home currency
r = Riskfree interest rate continuously compounded input as decimal 
V = Volatility - standard deviation of the underlying input as decimal
T = Tenor - time to expiration (% of year input as decimal)
