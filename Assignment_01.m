% This is a program that generates the ten anonymous functions:
% 
% Call price
% Put price
% Call delta
% Put delta
% Gamma
% Vega
% Call Theta
% Put Theta
% Call Rho
% Put Rho
%
% S = Underlying price per share in home currency
% K = Strike price per share in home currency
% r = Riskfree interest rate continuously compounded input as decimal 
% V = Volatility - standard deviation of the underlying input as decimal
% T = Tenor - time to expiration input as decimal years

CallPrice=@(S,K,r,V,T)S*cdf('norm',d1(S,K,r,V,T),0,1)-K*exp(-r*T)*cdf('norm',d2(S,K,r,V,T),0,1); % generates theoretical European Call price
PutPrice=@(S,K,r,V,T)K*exp(-r*T)*cdf('norm',-d2(S,K,r,V,T),0,1)-S*cdf('norm',-d1(S,K,r,V,T),0,1); % generates theoretical European Put price
CallDelta=@(S,K,r,V,T)cdf('norm',d1(S,K,r,V,T),0,1); % generates theoretical Call Delta
PutDelta=@(S,K,r,V,T)cdf('norm',d1(S,K,r,V,T),0,1)-1; % generates theoretical Put Delta
Gamma=@(S,K,r,V,T)pdf('norm',d1(S,K,r,V,T),0,1)/(S*V*(T^0.5)); % generates theoretical Gamma
Vega=@(S,K,r,V,T)S*pdf('norm',d1(S,K,r,V,T),0,1)*(T^0.5); % generates theoretical Vega
CallTheta=@(S,K,r,V,T)-(S*pdf('norm',d1(S,K,r,V,T),0,1)*V)/(2*(T^0.5))-r*K*exp(-r*T)*cdf('norm',d2(S,K,r,V,T),0,1); % generates theoretical Call Theta
PutTheta=@(S,K,r,V,T)-(S*pdf('norm',d1(S,K,r,V,T),0,1)*V)/(2*(T^0.5))+r*K*exp(-r*T)*cdf('norm',-d2(S,K,r,V,T),0,1); % generates theoretical Put Theta
CallRho=@(S,K,r,V,T)K*T*exp(-r*T)*cdf('norm',d2(S,K,r,V,T),0,1); % generates theoretical Call Rho
PutRho=@(S,K,r,V,T)-K*T*exp(-r*T)*cdf('norm',-d2(S,K,r,V,T),0,1); % generates theoretical Put Rho