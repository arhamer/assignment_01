% This is a function that is called by the anonymous functions. It
% calculates the input d2.

function [d2]=d2(S,K,r,V,T) % variables are passed from Assignment_01
    d2=(log(S/K)+(r-((V^2)/2))*T)/(V*(T^.5));
end