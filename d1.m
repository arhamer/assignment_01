% This is a function that is called by the anonymous functions. It
% calculates the input d1.

function [d1]=d1(S,K,r,V,T) % variables are passed from Assignment_01
    d1=(log(S/K)+(r+((V^2)/2))*T)/(V*(T^.5));
end